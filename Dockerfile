FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

ADD target/biblioteca-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]

EXPOSE 8081
#CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
