package com.biblioteca;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/*@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = "com.biblioteca")
@EntityScan(basePackages = "com.biblioteca")
@PropertySource("application.properties")
@EnableTransactionManagement*/
public class JpaConfig {
    /*
     * @Value("${spring.datasource.driver.class}")
     * private String driverClassName;
     * 
     * @Value("${spring.datasource.url}")
     * private String url;
     * 
     * @Value("${spring.datasource.username}")
     * private String usuario;
     * 
     * @Value("${spring.datasource.password}")
     * private String clave;
     * 
     * @Bean
     * public DataSource dataSource() {
     * DriverManagerDataSource dataSource = new DriverManagerDataSource();
     * dataSource.setDriverClassName(driverClassName);
     * dataSource.setUrl(url);
     * dataSource.setUsername(usuario);
     * dataSource.setPassword(clave);
     * return dataSource;
     * }
     * 
     * @Bean
     * public EntityManagerFactory entityManagerFactory() {
     * 
     * HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
     * vendorAdapter.setGenerateDdl(true);
     * 
     * LocalContainerEntityManagerFactoryBean factory = new
     * LocalContainerEntityManagerFactoryBean();
     * factory.setJpaVendorAdapter(vendorAdapter);
     * factory.setPackagesToScan("com.biblioteca");
     * factory.setDataSource(dataSource());
     * factory.afterPropertiesSet();
     * 
     * return factory.getObject();
     * }
     * 
     * @Bean
     * public PlatformTransactionManager transactionManager() {
     * 
     * JpaTransactionManager txManager = new JpaTransactionManager();
     * txManager.setEntityManagerFactory(entityManagerFactory());
     * return txManager;
     * }
     */
}
