package com.biblioteca.modelo;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "libro")
@Getter
@Setter
public class Libro implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(length = 200)
    private String autores;

    @Column(length = 250)
    private String titulo;
    @Column(length = 4, name = "num_pag", columnDefinition = "integer(4) default 0")
    private Integer numPag;
    @Column(length = 4, columnDefinition = "integer(4) default 2000")
    private Integer anio;

    @Column(columnDefinition = "varchar(36) default uuid()")
    private String external_id;

    @Column(length = 100)
    private String editorial;

    @Column(length = 100)
    private String edision;
    @Column(columnDefinition = "varchar(30) default 'NO_DATA'")
    private String isbn;
    @Column(columnDefinition = "varchar(5) default '00000'")
    private String codigo;
    @CreatedDate
    @Column(name = "create_at", updatable = false, columnDefinition = "datetime default now()")
    private Date createAt;
    @LastModifiedDate
    @Column(name = "update_at", columnDefinition = "datetime default now()")
    private Date updateAt;
    @OneToMany(mappedBy = "libro", cascade = CascadeType.ALL)
    private List<DetallePrestamo> detallesPrestamos;

    @Override
    public String toString() {
        return titulo + " numPag " + numPag;
    }
}
