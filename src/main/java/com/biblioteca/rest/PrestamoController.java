package com.biblioteca.rest;

import java.util.List;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.biblioteca.controladores.LibroRepository;
import com.biblioteca.controladores.PersonaRepository;
import com.biblioteca.controladores.PrestamoRepository;
import com.biblioteca.modelo.DetallePrestamo;
import com.biblioteca.modelo.Libro;
import com.biblioteca.modelo.Persona;
import com.biblioteca.modelo.Prestamo;
import com.biblioteca.rest.modelo_rest.DetalleWS;
import com.biblioteca.rest.modelo_rest.PrestamoWS;
import com.biblioteca.rest.respuesta.RespuestaLista;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
public class PrestamoController {
    @Autowired
    private PrestamoRepository prestamoRepository;
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private LibroRepository libroRepository;

    @GetMapping("/prestamos")
    public ResponseEntity listar() {
        List<Prestamo> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        prestamoRepository.findAllByOrderByFechaDesc().forEach((p) -> lista.add(p));
        Integer cont = 0;
        for (Prestamo p : lista) {
            cont++;
            HashMap aux = new HashMap<>();
            aux.put("id_persona", p.getPersona().getExternal_id());
            aux.put("persona", p.getPersona().getApellidos() + " " + p.getPersona().getNombres());
            aux.put("fecha", p.getFecha().toString());
            aux.put("identificacion", p.getPersona().getIdentificacion());
            List<HashMap> listaDetalle = new ArrayList<>();
            for (DetallePrestamo dp : p.getDetallesPrestamos()) {
                HashMap auxM = new HashMap<>();
                auxM.put("id_libro", dp.getLibro().getExternal_id());
                auxM.put("codigo", dp.getLibro().getCodigo());
                auxM.put("autor", dp.getLibro().getAutores());
                auxM.put("titulo", dp.getLibro().getTitulo());
                auxM.put("descripcion", dp.getLibro().getAnio() + " " + dp.getLibro().getEdision() + " "
                        + dp.getLibro().getEditorial() + " " + dp.getLibro().getIsbn());
                listaDetalle.add(auxM);
            }
            aux.put("detalle", listaDetalle);
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    @PostMapping("/prestamo/guardar")
    public ResponseEntity guardar(@Valid @RequestBody PrestamoWS prestamoWS) {

        HashMap mapa = new HashMap<>();
        Prestamo prestamo = new Prestamo();
        Persona p = personaRepository.findByExternal_id(prestamoWS.getExternal_persona());
        if (p != null) {
            prestamo.setCreateAt(new Date());
            prestamo.setUpdateAt(new Date());
            prestamo.setPersona(p);
            prestamo.setFecha(new Date());
            prestamo.setCodigo("000-" + String.valueOf(prestamoRepository.count()));
            prestamo.setEstado(true);
            // PARA predtamo
            if (!prestamoWS.getLista().isEmpty()) {
                try {
                    for (DetalleWS dw : prestamoWS.getLista()) {
                        DetallePrestamo dp = new DetallePrestamo();
                        Libro libro = libroRepository.findByExternal_id(dw.getExternal_libro());
                        System.out.println("Error ******** " + dw.getExternal_libro());
                        dp.setCreateAt(new Date());
                        dp.setUpdateAt(new Date());
                        dp.setLibro(libro);
                        dp.setPrestamo(prestamo);
                        prestamo.getDetallesPrestamos().add(dp);
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    // System.out.println("Error ******** " + e);
                    // e.printStackTrace();
                    mapa.put("evento", "Libro no encontrado");
                    return RespuestaLista.respuesta(mapa, "No se encontro el libro asociado a la lista");
                }
            } else {
                mapa.put("evento", "Lista de detalles no encontrado");
                return RespuestaLista.respuesta(mapa, "No se encontro el detalle del prestamo");
            }

            prestamoRepository.save(prestamo);
            mapa.put("evento", "Se ha registrado correctamente");
            return RespuestaLista.respuesta(mapa, "OK");
        } else {
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro el tipo de persona deseado");
        }

    }

    @GetMapping("/prestamos/persona/{external}")
    public ResponseEntity listarPersona(@PathVariable String external) {
        // prestamoRepository.buscarExternal_id(external);
        Persona pe = personaRepository.findByExternal_id(external);
        List<Prestamo> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        prestamoRepository.buscarExternal_id(external).forEach((p) -> lista.add(p));
        // prestamoRepository.findByPersona(pe).forEach((p) -> lista.add(p));
        Integer cont = 0;
        for (Prestamo p : lista) {
            cont++;
            HashMap aux = new HashMap<>();
            aux.put("id_persona", p.getPersona().getExternal_id());
            aux.put("persona", p.getPersona().getApellidos() + " " + p.getPersona().getNombres());
            aux.put("fecha", p.getFecha().toString());
            aux.put("identificacion", p.getPersona().getIdentificacion());
            List<HashMap> listaDetalle = new ArrayList<>();
            for (DetallePrestamo dp : p.getDetallesPrestamos()) {
                HashMap auxM = new HashMap<>();
                auxM.put("id_libro", dp.getLibro().getExternal_id());
                auxM.put("codigo", dp.getLibro().getCodigo());
                auxM.put("autor", dp.getLibro().getAutores());
                auxM.put("titulo", dp.getLibro().getTitulo());
                auxM.put("descripcion", dp.getLibro().getAnio() + " " + dp.getLibro().getEdision() + " "
                        + dp.getLibro().getEditorial() + " " + dp.getLibro().getIsbn());
                listaDetalle.add(auxM);
            }
            aux.put("detalle", listaDetalle);
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

}
