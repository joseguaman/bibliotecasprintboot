package com.biblioteca.rest.modelo_rest;

import java.util.Date;
import java.util.UUID;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.biblioteca.modelo.Cuenta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CuentaWS {
    @NotNull(message = "El correo no puede ser nulo")
    @Email(message = "Debe insertar un correo valido")
    private String correo;
    @NotBlank(message = "La clave es requerida")
    @NotNull(message = "La clave no puede ser nula")
    @NotEmpty(message = "La clave no puede estar vacia")
    private String clave;

    public Cuenta cargarObjeto(Cuenta cuenta) {
        if (cuenta == null)
            cuenta = new Cuenta();

        cuenta.setCorreo(correo);
        cuenta.setExternal_id(UUID.randomUUID().toString());
        cuenta.setUpdateAt(new Date());
        return cuenta;
    }
}
