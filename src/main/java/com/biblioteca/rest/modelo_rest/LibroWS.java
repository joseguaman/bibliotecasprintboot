package com.biblioteca.rest.modelo_rest;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.biblioteca.modelo.Libro;

@Getter
@Setter
public class LibroWS {
    @NotBlank(message = "Campo titulo es requerido")
    @Size(min = 3, max = 250)
    private String titulo;
    @NotBlank(message = "Campo autor es requerido")
    @Size(min = 3, max = 200)
    private String autor;
    private String external;
    private Integer anio;
    private Integer num_pag;
    @NotBlank(message = "Campo edicion es requerido")
    @Size(min = 1, max = 100, message = "El campo edision debe tener entre 3 y 100 caracteres")
    private String edision;
    @NotBlank(message = "Campo editorial es requerido")
    @Size(min = 3, max = 100)
    private String editorial;
    @NotBlank(message = "Campo isbn es requerido")
    @Size(min = 3, max = 30)
    private String isbn;

    public Libro cargarDatos(Libro libro) {
        // Libro libro = new Libro();
        if (libro == null)
            libro = new Libro();
        libro.setAnio(anio);
        libro.setAutores(autor);
        libro.setUpdateAt(new Date());
        libro.setEdision(edision);
        libro.setEditorial(editorial);
        libro.setExternal_id(UUID.randomUUID().toString());
        libro.setIsbn(isbn);
        libro.setNumPag(num_pag);
        libro.setTitulo(titulo);
        return libro;
    }

}
