package com.biblioteca.rest.modelo_rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.biblioteca.modelo.DetallePrestamo;
import com.biblioteca.modelo.Persona;
import com.biblioteca.modelo.Prestamo;

public class PrestamoWS {
    private String codigo;
    @NotBlank(message = "Campo de persona es requerido")
    private String external_persona;

    private List<@Valid DetalleWS> lista = new ArrayList<>();

    public List<DetalleWS> getLista() {
        return this.lista;
    }

    public void setLista(List<DetalleWS> lista) {
        this.lista = lista;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getExternal_persona() {
        return this.external_persona;
    }

    public void setExternal_persona(String external_persona) {
        this.external_persona = external_persona;
    }

}
