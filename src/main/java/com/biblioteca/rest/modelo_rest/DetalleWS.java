package com.biblioteca.rest.modelo_rest;

import javax.validation.constraints.NotBlank;

public class DetalleWS {
    @NotBlank(message = "Campo de libro es requerido")
    private String external_libro;

    public String getExternal_libro() {
        return this.external_libro;
    }

    public void setExternal_libro(String external_libro) {
        this.external_libro = external_libro;
    }

}
