package com.biblioteca.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.biblioteca.controladores.PersonaRepository;
import com.biblioteca.controladores.RolRepository;
import com.biblioteca.controladores.utiles.Utilidades;
import com.biblioteca.modelo.Cuenta;
import com.biblioteca.modelo.Persona;
import com.biblioteca.modelo.Rol;
import com.biblioteca.rest.modelo_rest.CuentaWS;
import com.biblioteca.rest.modelo_rest.PersonaWS;
import com.biblioteca.rest.respuesta.RespuestaLista;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
public class PersonaController {
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private RolRepository rolRepository;

    @GetMapping("/personas")
    public ResponseEntity listar() {
        List<Persona> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        personaRepository.findAll().forEach((p) -> lista.add(p));
        Integer cont = 0;
        for (Persona p : lista) {
            cont++;
            HashMap aux = new HashMap<>();
            aux.put("apellidos", p.getApellidos());
            aux.put("nombres", p.getNombres());
            aux.put("direccion", p.getDireccion());
            aux.put("external", p.getExternal_id());
            aux.put("identificacion", p.getIdentificacion());
            aux.put("tipo", p.getTipo().toString());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    @PostMapping("/personas/guardar")
    public ResponseEntity guardar(@Valid @RequestBody PersonaWS personaWS) {

        HashMap mapa = new HashMap<>();
        Persona persona = personaWS.cargarObjeto(null);
        Rol rol = rolRepository.findByNombre(personaWS.getTipo_persona().toLowerCase());
        if (rol != null) {
            persona.setCreateAt(new Date());
            persona.setRol(rol);
            // PARA CUENTA
            if (personaWS.getCuenta() != null) {
                Cuenta cuenta = personaWS.getCuenta().cargarObjeto(null);
                // System.out.println(personaWS.getCuenta().getClave());
                cuenta.setClave(Utilidades.clave(personaWS.getCuenta().getClave()));
                // System.out.println(cuenta.getClave());
                cuenta.setEstado(Boolean.TRUE);
                cuenta.setPersona(persona);
                cuenta.setCreateAt(new Date());
                persona.setCuenta(cuenta);
                // System.out.println("CUENTA");
            }
            // FIN CUENTA
            personaRepository.save(persona);
            mapa.put("evento", "Se ha registrado correctamente");
            return RespuestaLista.respuesta(mapa, "OK");
        } else {
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro el tipo de persona deseado");
        }

    }

}
