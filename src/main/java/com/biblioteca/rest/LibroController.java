package com.biblioteca.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.biblioteca.controladores.LibroRepository;
import com.biblioteca.modelo.Libro;
import com.biblioteca.rest.modelo_rest.LibroWS;
import com.biblioteca.rest.respuesta.RespuestaLista;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
public class LibroController {
    @Autowired
    private LibroRepository libroRepository;
    public static Integer AUTORES = 1;
    public static Integer TITULO = 2;
    public static Integer TODOS = 0;

    @GetMapping("/libros")
    public ResponseEntity listar() {
        List<Libro> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        libroRepository.findAll().forEach((p) -> lista.add(p));
        Integer cont = 0;
        for (Libro p : lista) {
            cont++;
            HashMap aux = new HashMap<>();
            aux.put("autores", p.getAutores());
            aux.put("titulo", p.getTitulo());
            aux.put("edision", p.getEdision());
            aux.put("external", p.getExternal_id());
            aux.put("editorial", p.getEditorial());
            aux.put("anio", p.getAnio());
            aux.put("isbn", p.getIsbn());
            aux.put("codigo", p.getCodigo());
            aux.put("creado_en", p.getCreateAt());
            aux.put("actualizado_en", p.getUpdateAt());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    @PostMapping("/libros/guardar")
    public ResponseEntity guardar(@Valid @RequestBody LibroWS librows) {
        HashMap mapa = new HashMap<>();
        Libro libro = librows.cargarDatos(null);

        libro.setCodigo(String.valueOf(libroRepository.count() + 1));

        libro.setCreateAt(new Date());
        mapa.put("evento", "Se ha registrado correctamente");
        libroRepository.save(libro);
        return RespuestaLista.respuesta(mapa, "OK");

    }

    @GetMapping("/libros/obtener/{external}")
    public ResponseEntity obtener(@PathVariable String external) {
        Libro libro = libroRepository.findByExternal_id(external);
        if (libro != null) {
            HashMap aux = new HashMap<>();
            aux.put("autores", libro.getAutores());
            aux.put("titulo", libro.getTitulo());
            aux.put("edision", libro.getEdision());
            aux.put("external", libro.getExternal_id());
            aux.put("editorial", libro.getEditorial());
            aux.put("anio", libro.getAnio());
            aux.put("isbn", libro.getIsbn());
            aux.put("codigo", libro.getCodigo());
            aux.put("creado_en", libro.getCreateAt());
            aux.put("actualizado_en", libro.getUpdateAt());
            return RespuestaLista.respuestaLista(aux);
        } else {
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro el obejto deseado");
        }
    }

    @PostMapping("/libros/editar")
    public ResponseEntity modificar(@Valid @RequestBody LibroWS librows) {
        Libro libro = libroRepository.findByExternal_id(librows.getExternal());
        if (libro != null) {
            HashMap mapa = new HashMap<>();
            libro = librows.cargarDatos(libro);
            mapa.put("evento", "Se ha modificado correctamente");
            libroRepository.save(libro);
            return RespuestaLista.respuesta(mapa, "OK");
        } else {
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro el obejto deseado");
        }

    }

    @GetMapping("/libros/buscar/{tipo}/{texto}")
    public ResponseEntity buscarAutores(@PathVariable Integer tipo, @PathVariable String texto) {
        List<Libro> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        if (tipo == 1)
            libroRepository.findByAutoresStartsWith(texto).forEach((p) -> lista.add(p));
        if (tipo == 2)
            libroRepository.findByTituloStartsWith(texto).forEach((p) -> lista.add(p));
        else
            libroRepository.findAll().forEach((p) -> lista.add(p));

        // Integer cont = 0;
        for (Libro p : lista) {
            // cont++;
            HashMap aux = new HashMap<>();
            aux.put("autores", p.getAutores());
            aux.put("titulo", p.getTitulo());
            aux.put("edision", p.getEdision());
            aux.put("external", p.getExternal_id());
            aux.put("editorial", p.getEditorial());
            aux.put("anio", p.getAnio());
            aux.put("isbn", p.getIsbn());
            aux.put("codigo", p.getCodigo());
            aux.put("creado_en", p.getCreateAt());
            aux.put("actualizado_en", p.getUpdateAt());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

}
