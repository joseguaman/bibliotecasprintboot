package com.biblioteca;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.biblioteca.security.JWTMiddleFilter;

@Configuration
public class RestSecurityConfig {
        @Bean
        public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

                http.csrf().disable().cors().and()
                                .addFilterAfter(new JWTMiddleFilter(),
                                                UsernamePasswordAuthenticationFilter.class)
                                .authorizeRequests()
                                .antMatchers(HttpMethod.POST, "/api/v1/inicio_sesion").permitAll()
                                .anyRequest().authenticated();
                return http.build();
        }
        /*
         * @Bean
         * 
         * @Profile("test")
         * public SecurityFilterChain filterChainDisabled(HttpSecurity http) throws
         * Exception {
         * 
         * http.csrf().disable()
         * .addFilterAfter(new JWTMiddleFilter(),
         * UsernamePasswordAuthenticationFilter.class)
         * .authorizeRequests()
         * .anyRequest().permitAll();
         * return http.build();
         * }
         */
}
