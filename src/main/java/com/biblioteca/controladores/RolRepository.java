package com.biblioteca.controladores;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.biblioteca.modelo.Rol;

public interface RolRepository extends CrudRepository<Rol, Integer> {
    @Query("SELECT r from Rol r WHERE r.nombre = ?1")
    Rol findByNombre(String nombre);
}
