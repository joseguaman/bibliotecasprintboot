package com.biblioteca.controladores;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.biblioteca.modelo.Persona;

public interface PersonaRepository extends CrudRepository<Persona, Integer> {

    @Query("SELECT p FROM com.biblioteca.modelo.Persona p WHERE p.external_id = ?1")
    Persona findByExternal_id(String external_id);
}
