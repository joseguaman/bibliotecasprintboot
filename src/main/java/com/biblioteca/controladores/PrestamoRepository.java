package com.biblioteca.controladores;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.biblioteca.modelo.Persona;
import com.biblioteca.modelo.Prestamo;

public interface PrestamoRepository extends CrudRepository<Prestamo, Integer> {
    @Query("SELECT p from Prestamo p WHERE p.external_id = ?1")
    Prestamo findByExternal_id(String external_id);

    @Query("SELECT p from Prestamo p WHERE p.persona.external_id = ?1")
    List<Prestamo> buscarExternal_id(String external_id);

    List<Prestamo> findByPersona(Persona persona);

    List<Prestamo> findAllByOrderByFechaDesc();
    // List<Prestamo> findByFecha(Date fecha);
}
