package com.biblioteca.controladores;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.biblioteca.modelo.Libro;

public interface LibroRepository extends CrudRepository<Libro, Integer> {
    @Query("SELECT l from com.biblioteca.modelo.Libro l WHERE l.external_id = ?1")
    Libro findByExternal_id(String external_id);

    List<Libro> findByAutoresStartsWith(String autore);

    List<Libro> findByTituloStartsWith(String titulo);
}
