package com.biblioteca;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.biblioteca.controladores.LibroRepository;
import com.biblioteca.modelo.Libro;
import com.biblioteca.rest.LibroController;
import com.biblioteca.rest.modelo_rest.LibroWS;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
// @ActiveProfiles("test")
public class LibroTest {
    @InjectMocks
    private LibroController libroController;
    @Mock
    private LibroWS librows;
    @Mock
    private LibroRepository libroRepository;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void listar() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/v1/libros")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void listarTodos() throws Exception {

        when(libroRepository.findAll()).thenReturn(lista());
        libroController.listar();
        verify(libroRepository).findAll();

    }

    @Test
    public void guardar() throws Exception {

        Libro libro = new Libro();
        LibroWS libroWS = new LibroWS();
        libro.setAutores("test");
        libro.setAnio(2002);
        libro.setEdision("test");
        libro.setEditorial("test");
        libro.setIsbn("test");
        libro.setTitulo("test");
        libro.setNumPag(45);
        libro.setUpdateAt(new Date());
        libro.setCodigo("3");
        libro.setCreateAt(new Date());

        libroWS.setAutor("test");
        libroWS.setAnio(2002);
        libroWS.setEdision("test");
        libroWS.setEditorial("test");
        libroWS.setIsbn("test");
        libroWS.setTitulo("test");
        libroWS.setNum_pag(45);

        when(librows.cargarDatos(ArgumentMatchers.any(Libro.class))).thenReturn(libro);

        when(libroRepository.count()).thenReturn(2l);
        libroController.guardar(libroWS);

        verify(libroRepository).count();

    }

    private List<Libro> lista() {
        List<Libro> lista = new ArrayList<>();
        Libro l = new Libro();
        l.setTitulo("java");
        l.setAutores("test 1");
        l.setCodigo("00001");
        l.setExternal_id("1");
        l.setCreateAt(new Date());
        l.setUpdateAt(new Date());
        l.setEdision("test");
        l.setEditorial("test");
        l.setAnio(2022);
        l.setIsbn("test");
        lista.add(l);
        Libro l1 = new Libro();
        l1.setTitulo("java 2");
        l1.setAutores("test 2");
        l1.setCodigo("00002");
        l1.setExternal_id("2");
        l1.setCreateAt(new Date());
        l1.setUpdateAt(new Date());
        l1.setEdision("test");
        l1.setEditorial("test");
        l1.setAnio(2022);
        l1.setIsbn("test");
        lista.add(l1);

        return lista;
    }
}
