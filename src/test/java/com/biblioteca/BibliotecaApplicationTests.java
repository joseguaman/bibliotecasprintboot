package com.biblioteca;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.Assert;

import com.biblioteca.controladores.CuentaRepository;
import com.biblioteca.controladores.PersonaRepository;
import com.biblioteca.controladores.PrestamoRepository;
import com.biblioteca.modelo.Cuenta;
import com.biblioteca.modelo.Persona;
import com.biblioteca.modelo.Rol;
import com.biblioteca.rest.CuentaController;
import com.biblioteca.rest.PersonaController;
import com.biblioteca.rest.PrestamoController;
import com.biblioteca.rest.modelo_rest.CuentaWS;
import com.biblioteca.rest.respuesta.RespuestaModelo;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;

@SpringBootTest
@AutoConfigureMockMvc

class BibliotecaApplicationTests {

	@InjectMocks
	private CuentaController cuentaController;
	@InjectMocks
	private PersonaController personaController;
	@InjectMocks
	private PrestamoController prestamoController;
	@Mock
	private CuentaWS cuentaws;
	@Mock
	private CuentaRepository cuentaRepository;
	@Mock
	private PersonaRepository personaRepository;
	@Mock
	private PrestamoRepository prestamoRepository;
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void listar() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/api/v1/prestamos")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError());

	}

	@Test
    public void listarTodosPersonas() throws Exception {

        when(personaRepository.findAll()).thenReturn(new ArrayList<>());
        personaController.listar();
        verify(personaRepository).findAll();

    }

	@Test
    public void listarTodosPrestamos() throws Exception {

        when(prestamoRepository.findAllByOrderByFechaDesc()).thenReturn(new ArrayList<>());
        prestamoController.listar();
        verify(prestamoRepository).findAllByOrderByFechaDesc();

    }

	@Test
	public void iniicioSesion() throws Exception  {

		 Persona p = new Persona();
		 p.setApellidos("Test");
		 Rol rol = new Rol();
		 rol.setNombre("admin");
		 Cuenta c = new Cuenta();

		 CuentaWS cws = new CuentaWS();
		 cws.setClave("Biblio123");
		 cws.setCorreo("biblio@admin.com");
		 c.setClave("$2a$10$S0M9yh2qEmdZIsKbwsbwI.cbGCPT.iNGOeMFRKpfmTFL8VAg.i.OK");
		 c.setCorreo("biblio@admin.com");
		 p.setCuenta(c);
		 p.setRol(rol);
		 c.setPersona(p);
		 //when(cws.cargarObjeto(c)).thenReturn(c);
		 //when(cuentaRepository.findByCorreo("biblio@admin.com")).thenReturn(c);

		 //ResponseEntity et = cuentaController.inicioSesion(cws);
		 //RespuestaModelo rem = (RespuestaModelo) et.getBody();

		 //HashMap mapa = (HashMap) rem.getData();

		 //Assert.isTrue(mapa.get("correo").toString().equalsIgnoreCase("biblio@admin.com"), "OK");

	}

}
